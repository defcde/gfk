﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Serv.Tests.SendLetterServiceReference;

namespace Serv.Tests
{
    [TestClass]
    public class SendLetterTest
    {
        [TestMethod]
        public void PdfGenerationTestPass()
        {
            SendLetterServiceClient client = new SendLetterServiceClient();
            Assert.IsTrue(client.GetLetter(1).LetterFile.Length > 1);
        }

        [TestMethod]
        public void PdfGenerationTestFail()
        {
        }
    }
}
