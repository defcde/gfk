﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Text;
using System.Web;
using Web.SendLetterService;

namespace Web.Utils
{
    public  static class EmailUtil
    {
        
            public static bool SendWelcome(LetterModel letter)
            {
            
                try
                {
                //
                var smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
                string username = smtpSection.Network.UserName;


                SmtpClient ss = new SmtpClient(smtpSection.Network.Host, smtpSection.Network.Port);
                    ss.EnableSsl = true;
                    ss.Timeout = 10000;
                    ss.DeliveryMethod = SmtpDeliveryMethod.Network;
                    ss.UseDefaultCredentials = false;
                    ss.Credentials = new NetworkCredential(smtpSection.Network.UserName, smtpSection.Network.Password);

                    Attachment att = new Attachment(new MemoryStream(letter.LetterFile), "welcome.pdf");

                    MailMessage mm = new MailMessage(smtpSection.Network.UserName, letter.Emailaddress, "Welcome to Hollard", String.Empty);
                    mm.BodyEncoding = UTF8Encoding.UTF8;
                    mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                    mm.Attachments.Add(att);
                    ss.Send(mm);
                    return true;
                    //
                }
                catch
                {
                    return false;
                }

            }

        }
    }
