﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.SendLetterService;
using Web.Utils;

namespace Web.Controllers
{
   // [Authorize]
    public class HomeController : Controller
    {
        //public SendLetterServiceClient service = new SendLetterServiceClient();

        public ISendLetterService service;
        public HomeController(ISendLetterService _service)
        {
            this.service = _service;
        }


        // GET: Home
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Index(int Id = 1)
        {
            LetterModel letter = service.GetLetter(Id);
            EmailUtil.SendWelcome(letter);
            
            return View();
        }



        // GET: SendLetter
        [HttpGet]
        //[Authorize]
        public ActionResult SendLetter(int Id = 1)
        {
           // string str = service.GetData(2);

           LetterModel letter = service.GetLetter(Id);
            
            //return View();
            string fileName = "myfile.pdf";
            return File(letter.LetterFile, System.Net.Mime.MediaTypeNames.Application.Pdf, fileName);
        }



        // GET: SendLetter
        [HttpPost]
        //[Authorize]
        public ActionResult SendLetter(SendLetterModel model)
        {
            if(ModelState.IsValid)
            {
                //Send Letter
            }
            return View();
        }


    }
}