﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Serv.DataInterfaces
{
    public interface IService<T> where T : class
    {
        List<T> GetAll();

        T GetFirst(int Id);

        bool Delete(int Id);

        T Insert(T Entity);
        T Update(T Entity);
    }
}