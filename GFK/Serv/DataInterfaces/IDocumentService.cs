﻿using DAL.Models;
using Serv.DataInterfaces;

namespace Serv.DataInterfaces
{
    public interface IDocumentService : IService<Document>
    {
    }
}