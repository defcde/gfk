﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Serv
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISendLetterService" in both code and config file together.
    [ServiceContract]
    public interface ISendLetterService
    {        
        [OperationContract]
        LetterModel GetLetter(int Id);
        
    }




    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class LetterModel
    {
        string _firstname = string.Empty;

        string _lastname = string.Empty;

        string _email = string.Empty;

        byte[] _letterFile = null;


        [DataMember]
        public string Firstname
        {
            get { return _firstname; }
            set { _firstname = value; }
        }

        [DataMember]
        public string Lastname
        {
            get { return _lastname; }
            set { _lastname = value; }
        }

        [DataMember]
        public string Emailaddress
        {
            get { return _email; }
            set { _email = value; }
        }

        [DataMember]
        public byte[] LetterFile
        {
            get { return _letterFile; }
            set { _letterFile = value; }
        }
    }



}
