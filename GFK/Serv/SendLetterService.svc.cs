﻿using System;
using System.Xml.Xsl;
using System.IO;
using System.Xml;
using Serv.DataInterfaces;
using System.Xml.Linq;
using SelectPdf;
using DAL.Models;

namespace Serv
{
    public class SendLetterService : ISendLetterService
    {
        
        private IPersonService PersonService;
        private IDocumentService DocumentService;
        public SendLetterService(IPersonService _PersonService, IDocumentService _DocumentService)
        {
            PersonService = _PersonService;
            DocumentService = _DocumentService;
        }

        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public LetterModel GetLetter(int Id)
        {
            LetterModel letter = new LetterModel();

            Person repo = PersonService.GetFirst(Id);
            letter.Firstname = repo.Firstname;
            letter.Lastname = repo.Lastname;
            letter.Emailaddress = repo.email;

          
            string output = String.Empty;
            
            try
            {
                string xslInput = File.ReadAllText(System.Web.Hosting.HostingEnvironment.MapPath("~/transform.xsl"));

                string xmlInput = new XElement("letter",
                    new XElement("addressline1", "P.O Box 652"),
                    new XElement("addressline2", "Auckland Park"),
                    new XElement("addressline3", "1255"),
                    new XElement("name", letter.Firstname),
                    new XElement("family-name", letter.Lastname),
                    new XElement("date", DateTime.Now.ToString("dd MMMM yyyy"))
                                ).ToString();                


                using (StringReader srt = new StringReader(xslInput)) // xslInput is a string that contains xsl
                using (StringReader sri = new StringReader(xmlInput)) // xmlInput is a string that contains xml
                {
                    using (XmlReader xrt = XmlReader.Create(srt))
                    using (XmlReader xri = XmlReader.Create(sri))
                    {
                        XslCompiledTransform xslt = new XslCompiledTransform();
                        xslt.Load(xrt);
                        using (StringWriter sw = new StringWriter())
                        using (XmlWriter xwo = XmlWriter.Create(sw, xslt.OutputSettings)) // use OutputSettings of xsl, so it can be output as HTML
                        {
                            xslt.Transform(xri, xwo);
                            output = sw.ToString();
                        }
                    }
                }

            }
            catch (Exception)
            {

            }
            
            HtmlToPdf converter = new HtmlToPdf();

            // set converter options
            converter.Options.PdfPageSize = PdfPageSize.Letter;
            converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait;
            converter.Options.AutoFitWidth = HtmlToPdfPageFitMode.AutoFit;
            converter.Options.MarginLeft = 50;
            converter.Options.MarginRight = 50;
            converter.Options.MarginTop = 50;
            converter.Options.MarginBottom = 50;
            //converter.Options.WebPageWidth = 1024;
            //converter.Options.WebPageHeight = webPageHeight;
            
            PdfDocument doc = converter.ConvertHtmlString(output);            
            letter.LetterFile = doc.Save();            
            doc.Close();

            Document printedDoc = new Document();
            printedDoc.PersonId = Id;
            printedDoc.PdfBin = letter.LetterFile;
            DocumentService.Insert(printedDoc);
            return letter;
        }
    }
}
