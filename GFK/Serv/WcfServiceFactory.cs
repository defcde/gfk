using DAL.Interfaces;
using DALFramework.Implementation;
using Microsoft.Practices.Unity;
using Serv.DataInterfaces;
using Serv.DataServices;
using Unity.Wcf;

namespace Serv
{
    public class WcfServiceFactory : UnityServiceHostFactory
    {
        protected override void ConfigureContainer(IUnityContainer container)
        {
            // register all your components with the container here
            // container
            //    .RegisterType<IService1, Service1>()
            //    .RegisterType<DataContext>(new HierarchicalLifetimeManager());

            // container.RegisterType<IPersonService, PersonService>();
            container                      
                          .RegisterType(typeof(IPersonService),
                        new InjectionFactory(c => new PersonService(new PersonImpl())))

                        //  .RegisterType(typeof(IPersonService),
                        //new InjectionFactory(c => new UserService(new UserImpl())))

                          .RegisterType(typeof(IUserService),
                        new InjectionFactory(c => new UserService(new UserImpl())))

                          .RegisterType(typeof(IDocumentService),
                        new InjectionFactory(c => new DocumentService(new DocumentImpl())))

                          .RegisterType(typeof(ILookupService),
                        new InjectionFactory(c => new LookupService(new LookupImpl())))

                          .RegisterType(typeof(ISendLetterService),
                        new InjectionFactory(c => new SendLetterService(new PersonService(new PersonImpl()),new DocumentService(new DocumentImpl()))));


      
        }
    }
}