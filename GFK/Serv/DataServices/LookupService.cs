﻿using DAL.Interfaces;
using DAL.Models;
using Serv.DataInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Serv.DataServices
{

    public class LookupService : ILookupService
    {
        private ILookup LookupImpl;
        public LookupService(ILookup _LookupImpl)
        {
            LookupImpl = _LookupImpl;
        }

        public List<Lookup> GetAll()
        {
            return LookupImpl.GetAll();
        }

        public Lookup GetFirst(int LookupId)
        {
            return LookupImpl.GetFirst(LookupId);
        }

        public bool Delete(int LookupId)
        {
            return LookupImpl.Delete(LookupId);
        }

        public Lookup Insert(Lookup Lookup)
        {
            return LookupImpl.Insert(Lookup);
        }

        public Lookup Update(Lookup Lookup)
        {
            return LookupImpl.Update(Lookup);
        }
    }

}