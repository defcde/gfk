﻿using DAL.Interfaces;
using DAL.Models;
using DALFramework.Implementation;
using Serv.DataInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Serv.DataServices
{
    public class UserService : IUserService
    {
        private IUser UserImpl;
        public UserService(IUser _UserImpl)
        {
            UserImpl = _UserImpl;
        }

        public List<User> GetAll()
        {
            return UserImpl.GetAll();
        }

        public User GetFirst(int UserId)
        {
            return UserImpl.GetFirst(UserId);
        }

        public bool Delete(int UserId)
        {
            return UserImpl.Delete(UserId);
        }

        public User Insert(User User)
        {
            return UserImpl.Insert(User);
        }

        public User Update(User User)
        {
            return UserImpl.Update(User);
        }
    }


}