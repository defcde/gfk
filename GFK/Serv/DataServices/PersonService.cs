﻿using DAL.Interfaces;
using DAL.Models;
using DALFramework.Implementation;
using Serv.DataInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Serv.DataServices
{
    public class PersonService : IPersonService
    {
        private IPerson personImpl;
        public PersonService(IPerson _personImpl)
        {
            personImpl = _personImpl;
        }

        public List<Person> GetAll()
        {
            return personImpl.GetAll();
        }

        public Person GetFirst(int PersonId)
        {
            return personImpl.GetFirst(PersonId);
        }

        public bool Delete(int PersonId)
        {
            return personImpl.Delete(PersonId);
        }

        public Person Insert(Person person)
        {
            return personImpl.Insert(person);
        }

        public Person Update(Person person)
        {
            return personImpl.Update(person);
        }
    }


}