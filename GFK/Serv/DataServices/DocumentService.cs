﻿
using DAL.Interfaces;
using DAL.Models;
using Serv.DataInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Serv.DataServices
{
    public class DocumentService : IDocumentService
    {
        private IDocument documentImpl;
        public DocumentService(IDocument _documentImpl)
        {
            documentImpl = _documentImpl;
        }

        public List<Document> GetAll()
        {
            return documentImpl.GetAll();
        }

        public Document GetFirst(int DocumentId)
        {
            return documentImpl.GetFirst(DocumentId);
        }

        public bool Delete(int DocumentId)
        {
            return documentImpl.Delete(DocumentId);
        }

        public Document Insert(Document Document)
        {
            return documentImpl.Insert(Document);
        }

        public Document Update(Document Document)
        {
            return documentImpl.Update(Document);
        }
    }

}