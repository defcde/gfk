﻿using DAL.Interfaces;
using DAL.Models;
using DALFramework.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALFramework.Implementation
{
    public class LookupImpl : ILookup
    {
        string connectionString = ConnectionHelper.GetConnectionString();

        public LookupImpl()
        {

        }

        public List<Lookup> GetAll()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string queryString = "SELECT * FROM Lookup WHERE IsDeleted = 0";
                    SqlDataAdapter adapter = new SqlDataAdapter(queryString, connection);
                    connection.Open();
                    DataTable Lookups = new DataTable();
                    adapter.Fill(Lookups);

                    return Lookups.AsEnumerable().Select(r => new Lookup()
                    {
                        LookupId = (int)r["LookupId"],
                        Name = (string)r["Name"],
                        Value = (string)r["Value"]
                    }).ToList();
                }
            }
            catch
            {
                return null;
            }
        }

        public Lookup GetFirst(int Id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string queryString = "SELECT TOP 1 * FROM Lookup WHERE LookupId = @LookupId AND IsDeleted = 0";
                    SqlDataAdapter adapter = new SqlDataAdapter(queryString, connection);
                    connection.Open();
                    DataTable Lookups = new DataTable();
                    adapter.SelectCommand.Parameters.AddWithValue("@LookupId", Id);
                    adapter.Fill(Lookups);

                    return Lookups.AsEnumerable().Select(r => new Lookup()
                    {
                        LookupId = (int)r["LookupId"],
                        Name = (string)r["Name"],
                        Value = (string)r["Value"]
                    }).FirstOrDefault();
                }

            }
            catch
            {
                return null;
            }
        }

        public bool Delete(int LookupId)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string queryString = "DELETE FROM Lookup WHERE LookupId=@LookupId";
                    SqlCommand cmd = new SqlCommand(queryString, connection);
                    cmd.Parameters.AddWithValue("@LookupId", LookupId);
                    return cmd.ExecuteNonQuery() > 0;
                }
            }
            catch
            {
                return false;
            }
        }

        public Lookup Insert(Lookup Lookup)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("INSERT INTO Lookup(Name,Value) VALUES(@Name,@Value)", connection);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);

                    connection.Open();
                    DataTable Lookups = new DataTable();
                    adapter.SelectCommand.Parameters.AddWithValue("@Name", Lookup.Name);
                    adapter.SelectCommand.Parameters.AddWithValue("@Value", Lookup.Value);
                    adapter.Fill(Lookups);
                    cmd.ExecuteNonQuery();

                    return Lookups.AsEnumerable().Select(r => new Lookup()
                    {
                        LookupId = (int)r["LookupId"],
                        Name = (string)r["Name"],
                        Value = (string)r["Value"]
                    }).FirstOrDefault();
                }
            }
            catch
            {
                return null;
            }
        }

        public Lookup Update(Lookup Lookup)
        {
                try
                {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("UPDATE Lookup SET Name=@Name,Value=@Value WHERE LookupId=@LookupId", connection);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);

                    connection.Open();
                    DataTable Lookups = new DataTable();

                    adapter.SelectCommand.Parameters.AddWithValue("@LookupId", Lookup.LookupId);
                    adapter.SelectCommand.Parameters.AddWithValue("@Name", Lookup.Name);
                    adapter.SelectCommand.Parameters.AddWithValue("@Value", Lookup.Value);
                    adapter.Fill(Lookups);
                    cmd.ExecuteNonQuery();

                    return Lookups.AsEnumerable().Select(r => new Lookup()
                    {
                        LookupId = (int)r["LookupId"],
                        Name = (string)r["Name"],
                        Value = (string)r["Value"]
                    }).FirstOrDefault();
                }

                }
                catch
                {
                    return null;
                }            
        }
    }
}
