﻿using DAL.Interfaces;
using DAL.Models;
using DALFramework.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALFramework.Implementation
{
    public class DocumentImpl : IDocument
    {
        string connectionString = ConnectionHelper.GetConnectionString();

        public DocumentImpl()
        {

        }

        public List<Document> GetAll()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string queryString = "SELECT * FROM Document WHERE IsDeleted = 0";
                    SqlDataAdapter adapter = new SqlDataAdapter(queryString, connection);
                    connection.Open();
                    DataTable Documents = new DataTable();
                    adapter.Fill(Documents);

                    return Documents.AsEnumerable().Select(r => new Document()
                    {
                        DocumentId = (int)r["DocumentId"],
                        PersonId = (int)r["PersonId"],
                        PdfBin = (byte[])r["PdfBin"]
                    }).ToList();
                }
            }
            catch
            {
                return null;
            }
        }

        public Document GetFirst(int Id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string queryString = "SELECT TOP 1 * FROM Document WHERE DocumentId = @DocumentId AND IsDeleted = 0";
                    SqlDataAdapter adapter = new SqlDataAdapter(queryString, connection);
                    connection.Open();
                    DataTable Documents = new DataTable();
                    adapter.SelectCommand.Parameters.AddWithValue("@DocumentId", Id);
                    adapter.Fill(Documents);

                    return Documents.AsEnumerable().Select(r => new Document()
                    {
                        DocumentId = (int)r["DocumentId"],
                        PersonId = (int)r["PersonId"],
                        PdfBin = (byte[])r["PdfBin"]
                    }).FirstOrDefault();
                }

            }
            catch
            {
                return null;
            }
        }

        public bool Delete(int DocumentId)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string queryString = "DELETE FROM Document WHERE DocumentId=@DocumentId";
                    SqlCommand cmd = new SqlCommand(queryString, connection);
                    cmd.Parameters.AddWithValue("@DocumentId", DocumentId);
                    return cmd.ExecuteNonQuery() > 0;
                }
            }
            catch
            {
                return false;
            }
        }

        public Document Insert(Document Document)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("INSERT INTO Document(PersonId,PdfBin) VALUES(@PersonId,@PdfBin)", connection);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);

                    connection.Open();
                    DataTable Documents = new DataTable();
                    adapter.SelectCommand.Parameters.AddWithValue("@PersonId", Document.PersonId);
                    adapter.SelectCommand.Parameters.AddWithValue("@PdfBin", Document.PdfBin);
                    adapter.Fill(Documents);
                    cmd.ExecuteNonQuery();

                    return Documents.AsEnumerable().Select(r => new Document()
                    {
                        DocumentId = (int)r["DocumentId"],
                        PersonId = (int)r["PersonId"],
                        PdfBin = (byte[])r["PdfBin"]
                    }).FirstOrDefault();
                }
            }
            catch
            {
                return null;
            }
        }

        public Document Update(Document Document)
        {            
                try
                {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("UPDATE Document SET PersonId=@PersonId,PdfBin=@PdfBin WHERE DocumentId=@DocumentId", connection);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);

                    connection.Open();
                    DataTable Documents = new DataTable();

                    adapter.SelectCommand.Parameters.AddWithValue("@DocumentId", Document.DocumentId);
                    adapter.SelectCommand.Parameters.AddWithValue("@PersonId", Document.PersonId);
                    adapter.SelectCommand.Parameters.AddWithValue("@PdfBin", Document.PdfBin);
                    adapter.Fill(Documents);
                    cmd.ExecuteNonQuery();

                    return Documents.AsEnumerable().Select(r => new Document()
                    {
                        DocumentId = (int)r["DocumentId"],
                        PersonId = (int)r["PersonId"],
                        PdfBin = (byte[])r["PdfBin"]
                    }).FirstOrDefault();
                }
                }
                catch
                {
                    return null;
                }
            }        
    }
}
