﻿using DAL.Interfaces;
using DAL.Models;
using DALFramework.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALFramework.Implementation
{
    public class UserImpl : IUser
    {
        string connectionString = ConnectionHelper.GetConnectionString();

        public UserImpl()
        {

        }

        public List<User> GetAll()
        {
            try
            {

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string queryString = "SELECT * FROM User WHERE IsDeleted = 0";
                    SqlDataAdapter adapter = new SqlDataAdapter(queryString, connection);
                    connection.Open();
                    DataTable Users = new DataTable();
                    adapter.Fill(Users);

                    return Users.AsEnumerable().Select(r => new User()
                    {
                        UserId = (int)r["UserId"],
                        Firstname = (string)r["Firstname"],
                        Lastname = (string)r["Lastname"],
                        email = (string)r["Emailaddress"],
                    }).ToList();
                }

            }
            catch
            {
                return null;
            }

        }

        public User GetFirst(int UserId)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string queryString = "SELECT TOP 1 * FROM User WHERE UserId = @UserId AND IsDeleted = 0";
                    SqlDataAdapter adapter = new SqlDataAdapter(queryString, connection);
                    connection.Open();
                    DataTable Users = new DataTable();
                    adapter.SelectCommand.Parameters.AddWithValue("@UserId", UserId);
                    adapter.Fill(Users);

                    return Users.AsEnumerable().Select(r => new User()
                    {
                        UserId = (int)r["UserId"],
                        Firstname = (string)r["Firstname"],
                        Lastname = (string)r["Lastname"],
                        email = (string)r["Emailaddress"],
                    }).FirstOrDefault();
                }

            }
            catch
            {
                return null;
            }
        }

        public bool Delete(int UserId)
        {

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string queryString = "DELETE FROM User WHERE UserId=@UserId";
                    SqlCommand cmd = new SqlCommand(queryString, connection);
                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    return cmd.ExecuteNonQuery() > 0;
                }
            }
            catch
            {
                return false;
            }

        }


        public User Insert(User User)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("INSERT INTO User(Firstname,Lastname,Emailaddress) VALUES(@Firstname,@Lastname,@email)", connection);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);

                    connection.Open();
                    DataTable Users = new DataTable();
                    adapter.SelectCommand.Parameters.AddWithValue("@Firstname", User.Firstname);
                    adapter.SelectCommand.Parameters.AddWithValue("@Lastname", User.Lastname);
                    adapter.SelectCommand.Parameters.AddWithValue("@email", User.email);
                    adapter.Fill(Users);
                    cmd.ExecuteNonQuery();

                    return Users.AsEnumerable().Select(r => new User()
                    {
                        UserId = (int)r["UserId"],
                        Firstname = (string)r["Firstname"],
                        Lastname = (string)r["Lastname"],
                        email = (string)r["Emailaddress"],
                    }).FirstOrDefault();
                }
            }
            catch
            {
                return null;
            }
        }

        public User Update(User User)
        {
                try
                {

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("UPDATE User SET Firstname=@Firstname,Lastname=@Lastname,Emailaddress=@email WHERE UserId=@UserId", connection);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);

                    connection.Open();
                    DataTable Users = new DataTable();
                    adapter.SelectCommand.Parameters.AddWithValue("@UserId", User.UserId);
                    adapter.SelectCommand.Parameters.AddWithValue("@Firstname", User.Firstname);
                    adapter.SelectCommand.Parameters.AddWithValue("@Lastname", User.Lastname);
                    adapter.SelectCommand.Parameters.AddWithValue("@email", User.email);
                    adapter.Fill(Users);
                    cmd.ExecuteNonQuery();

                    return Users.AsEnumerable().Select(r => new User()
                    {
                        UserId = (int)r["UserId"],
                        Firstname = (string)r["Firstname"],
                        Lastname = (string)r["Lastname"],
                        email = (string)r["Emailaddress"],
                    }).FirstOrDefault();
                }

                }
                catch
                {
                    return null;
                }
            }
        }
    }

