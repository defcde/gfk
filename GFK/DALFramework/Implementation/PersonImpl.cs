﻿using DAL.Interfaces;
using DAL.Models;
using DALFramework.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALFramework.Implementation
{
    public class PersonImpl : IPerson
    {
        string connectionString = ConnectionHelper.GetConnectionString();

        public PersonImpl()
        {

        }

        public List<Person> GetAll()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string queryString = "SELECT * FROM Person WHERE IsDeleted = 0";
                    SqlDataAdapter adapter = new SqlDataAdapter(queryString, connection);
                    connection.Open();
                    DataTable persons = new DataTable();
                    adapter.Fill(persons);

                    return persons.AsEnumerable().Select(r => new Person()
                    {
                        PersonId = (int)r["PersonId"],
                        Firstname = (string)r["Firstname"],
                        Lastname = (string)r["Lastname"],
                        email = (string)r["Emailaddress"],
                    }).ToList();
                }
            }
            catch
            {
                return null;
            }
        }

        public Person GetFirst(int Id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string queryString = "SELECT TOP 1 * FROM Person WHERE PersonId = @PersonId AND IsDeleted = 0";
                    SqlDataAdapter adapter = new SqlDataAdapter(queryString, connection);
                    connection.Open();
                    DataTable persons = new DataTable();
                    adapter.SelectCommand.Parameters.AddWithValue("@PersonId", Id);
                    adapter.Fill(persons);

                    return persons.AsEnumerable().Select(r => new Person()
                    {
                        PersonId = (int)r["PersonId"],
                        Firstname = (string)r["Firstname"],
                        Lastname = (string)r["Lastname"],
                        email = (string)r["Emailaddress"],
                    }).FirstOrDefault();
                }

            }
            catch(Exception e)
            {
                return null;
            }
        }

        public bool Delete(int PersonId)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string queryString = "DELETE FROM Person WHERE PersonId=@PersonId";
                    SqlCommand cmd = new SqlCommand(queryString, connection);
                    cmd.Parameters.AddWithValue("@PersonId", PersonId);
                    return cmd.ExecuteNonQuery() > 0;
                }
            }
            catch
            {
                return false;
            }
        }
        
        public Person Insert(Person person)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("INSERT INTO Person(Firstname,Lastname,Emailaddress) VALUES(@Firstname,@Lastname,@email)", connection);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);

                    connection.Open();
                    DataTable persons = new DataTable();
                    adapter.SelectCommand.Parameters.AddWithValue("@Firstname", person.Firstname);
                    adapter.SelectCommand.Parameters.AddWithValue("@Lastname", person.Lastname);
                    adapter.SelectCommand.Parameters.AddWithValue("@email", person.email);
                    adapter.Fill(persons);
                    cmd.ExecuteNonQuery();

                    return persons.AsEnumerable().Select(r => new Person()
                    {
                        PersonId = (int)r["PersonId"],
                        Firstname = (string)r["Firstname"],
                        Lastname = (string)r["Lastname"],
                        email = (string)r["Emailaddress"],
                    }).FirstOrDefault();
                }
            }
            catch
            {
                return null;
            }
        }

        public Person Update(Person person)
        {            
                try
                {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("UPDATE Person SET Firstname=@Firstname,Lastname=@Lastname,Emailaddress=@email WHERE PersonId=@PersonId", connection);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);

                    connection.Open();
                    DataTable persons = new DataTable();

                    adapter.SelectCommand.Parameters.AddWithValue("@PersonId", person.PersonId);
                    adapter.SelectCommand.Parameters.AddWithValue("@Firstname", person.Firstname);
                    adapter.SelectCommand.Parameters.AddWithValue("@Lastname", person.Lastname);
                    adapter.SelectCommand.Parameters.AddWithValue("@email", person.email);
                    adapter.Fill(persons);
                    cmd.ExecuteNonQuery();

                    return persons.AsEnumerable().Select(r => new Person()
                    {
                        PersonId = (int)r["PersonId"],
                        Firstname = (string)r["Firstname"],
                        Lastname = (string)r["Lastname"],
                        email = (string)r["Emailaddress"],
                    }).FirstOrDefault();
                }

                }
                catch
                {
                    return null;
                }            
        }
    }
}
