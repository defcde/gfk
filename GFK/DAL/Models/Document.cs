﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class Document
    {
        public int DocumentId { get; set; }
        public int PersonId { get; set; }
        public byte[] PdfBin { get; set; }
        public bool IsDeleted { get; set; }

    }
}
