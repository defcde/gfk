﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class Lookup
    {
        public int LookupId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
