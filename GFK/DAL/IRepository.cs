﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
     public interface IRepository<T> where T : class
    {        
        List<T> GetAll();
        T GetFirst(int Id);
        bool Delete(int Id);
        T Insert(T Entity);
        T Update(T Entity);

    }
}
